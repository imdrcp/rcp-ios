//
//  RealmManager.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/7.
//  Copyright © 2018年 gs1. All rights reserved.
//

import Foundation
import RealmSwift

protocol UnmanagedCopy {
    func unmanagedCopy() -> Self
}

extension Object:UnmanagedCopy{
    func unmanagedCopy() -> Self {
        let o = type(of:self).init()
        for p in objectSchema.properties {
            let value = self.value(forKey: p.name)
            switch p.type {
            case .linkingObjects:
                break
            default:
                o.setValue(value, forKey: p.name)
            }
        }
        
        return o
    }
}


class RealmManager {
    let realm = try! Realm()
    
    func serialize<T:Object>(models: [T]) {
        for m in models {
            do {
                try realm.write {
                    realm.add(m, update: true)
                }
            }catch{
                
            }
        }
    }
    
    func createOrUpdate(model:Object,condition:((_ realm:Realm) -> Bool)? = nil)->Bool?{
        if let query = condition {
            let result = query(realm)
            guard result else {
                return false
            }
        }
        
        do {
            try realm.write {
                realm.add(model, update: true)
            }
        }catch{
            return false
        }
        
        return true
    }
    
    func createOrUpdate(model:Object.Type,value:Dictionary<String,Any> ,
                        condition:((_ realm:Realm) -> Bool)? = nil)->Bool?{
        if let query = condition {
            let result = query(realm)
            guard result else {
                return false
            }
        }
        
        do {
            try! realm.write {
                realm.create(model, value: value, update: true)
            }
        }
        
        return true
    }
    
    func delete(model:Object.Type,condition:((_ model:Object.Type) -> Object)? = nil)->Bool?{
        
        guard let data = condition else {
            return false
        }
        
        DispatchQueue.main.async {
            try! self.realm.write {
                self.realm.delete(data(model))
            }
        }
        
        return true
    }
    
    func deleteTableData(model: Object.Type)-> Bool {
        if let models = self.readAll(model: model) {
            DispatchQueue.main.async {
                try! self.realm.write {
                    self.realm.delete(models)
                }
            }
            return true
        }
        return false
    }
    
    func deleteDBData() {
        DispatchQueue.main.async {
            try! self.realm.write {
                self.realm.deleteAll()
            }
        }
    }
    
    func read(model:Object.Type,condition:NSPredicate?,sortBy:String?=nil,isASE:Bool=false)->Results<Object>?{
        
        var data:Results<Object>?
        
        
        if let query = condition {
            if let sort = sortBy{
                data = realm.objects(model).filter(query).sorted(byKeyPath: sort, ascending: isASE)
            }else{
                data = realm.objects(model).filter(query)
            }
        }else{
            data = realm.objects(model)
        }
        return data
        
        
    }
    
    func readAll(model:Object.Type,sortBy:String? = nil,isASE:Bool = false)->Results<Object>?{
        
        var data:Results<Object>?
        
        if let sort = sortBy{
            data = realm.objects(model).sorted(byKeyPath: sort, ascending: isASE)
        }else{
            data = realm.objects(model)
        }
        return data
        
        
    }
}

