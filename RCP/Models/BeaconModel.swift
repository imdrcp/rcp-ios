//
//  BeaconModel.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/11/2.
//  Copyright © 2018 gs1. All rights reserved.
//

import RealmSwift
import Realm


class BeaconModel: Object, Decodable {
    @objc dynamic var id: Int = 0
    @objc dynamic var beaconId = ""
    @objc dynamic var beaconUUID = ""
    @objc dynamic var beaconMajor: Int = 0
    @objc dynamic var beaconMinor: Int = 0
    @objc dynamic var isManual: Bool = false
    //curriculum
    @objc dynamic var curriculum: Int = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case beaconId = "beacon_id"
        case beaconUUID = "beacon_uuid"
        case beaconMajor = "beacon_major"
        case beaconMinor = "beacon_minor"
        case isManual = "is_manual"
        case curriculum = "curriculum"
    }
    
    convenience init(id: Int,
                     beaconId: String, beaconUUID: String,
                     beaconMajor: Int, beaconMinor: Int,
                     isManual: Bool, curriculum: Int) {
        self.init()
        self.id = id
        self.beaconId = beaconId
        self.beaconUUID = beaconUUID
        self.beaconMajor = beaconMajor
        self.beaconMinor = beaconMinor
        self.isManual = isManual
        self.curriculum = curriculum
    }
    
    convenience required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let id = try container.decode(Int.self, forKey: .id)
        let beaconId = try container.decode(String.self, forKey: .beaconId)
        let beaconMajor = try container.decode(Int.self, forKey: .beaconMajor)
        let beaconUUID = try container.decode(String.self, forKey: .beaconUUID)
        let beaconMinor = try container.decode(Int.self, forKey: .beaconMinor)
        let isManual = try container.decode(Bool.self, forKey: .isManual)
        let curriculum = try container.decode(Int.self, forKey: .curriculum)
        self.init(id: id, beaconId: beaconId,
                  beaconUUID: beaconUUID, beaconMajor: beaconMajor, beaconMinor: beaconMinor, isManual: isManual, curriculum: curriculum)
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
}
