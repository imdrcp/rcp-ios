//
//  BeaconRecordModel.swift
//  RCP
//
//  Created by User on 2018/11/23.
//  Copyright © 2018 gs1. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class BeaconRecordModel: Object, Decodable {
    @objc dynamic var id: Int = 0
    @objc dynamic var beacon: BeaconModel?
    @objc dynamic var sectionTime: SectionTimeModel?
    @objc dynamic var rollCallTime: String = ""
    
    convenience init(beacon: BeaconModel, sectionTime: SectionTimeModel, rollCallTime: String) {
        self.init()
        self.id = BeaconRecordModel.incrementID()
        self.beacon = beacon
        self.sectionTime = sectionTime
        self.rollCallTime = rollCallTime
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    private static func incrementID() -> Int {
        let realm = try! Realm()
        return (realm.objects(BeaconRecordModel.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    private static func setBeaconRecord(id: Int, beacon: BeaconModel, sectionTime: SectionTimeModel, rollCallTime: String) {
        
    }
    
    required convenience init(isIncrement: Bool) {
        self.init()
        if isIncrement {
            id = BeaconRecordModel.incrementID()
        }
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
}
