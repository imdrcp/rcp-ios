//
//  RealmMigration.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/7.
//  Copyright © 2018年 gs1. All rights reserved.
//
import Foundation
import RealmSwift

class RealmMigration {
    
    
    func didApplicationLunch () {
        self.migrationVersion()
    }
    
    func migrationVersion() {
        
        
        let config = Realm.Configuration(
            
            schemaVersion : 1 ,
            
            migrationBlock : { migration , oldSchemaVersion in
                
                //                if (oldSchemaVersion < 1) {
                //                    如果你有必須針對舊板本遷移到新板本的資料改變，就寫在這裡。
                //                    詳細的做法可以參考官方的範例
                //                }
                
        }
            
        )
        print(config.fileURL!)
        Realm.Configuration.defaultConfiguration = config
        
    }
    
}
