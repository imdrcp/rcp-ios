//
//  CurriculumModels.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/7.
//  Copyright © 2018年 gs1. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class SubjectModel: Object, Decodable {
    @objc dynamic var id: Int = 0
    let sectionTimes = List<SectionTimeModel>()
    @objc dynamic var subjectsName = ""
    @objc dynamic var classRoom: String? = nil
    @objc dynamic var year = ""
    @objc dynamic var semester: String? = nil
    @objc dynamic var teacher: String? = nil
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case sectionTimes = "section_times"
        case subjectsName = "subjects_name"
        case classRoom = "class_room"
        case year = "year"
        case semester = "semester"
        case teacher = "teacher"
    }
    
    convenience init(id: Int, sectionTimes: [SectionTimeModel],
                     subjectsName: String, classRoom: String?,
                     year: String, semester: String?,
                     teacher: String?) {
        self.init()
        self.id = id
        self.sectionTimes.append(objectsIn: sectionTimes)
        self.subjectsName = subjectsName
        self.classRoom = classRoom
        self.year = year
        self.semester = semester
        self.teacher = teacher
    }
    
    convenience required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let id = try container.decode(Int.self, forKey: .id)
        let sectionTimes = try container.decode([SectionTimeModel].self, forKey: .sectionTimes)
        let subjectsName = try container.decode(String.self, forKey: .subjectsName)
        let year = try container.decode(String.self, forKey: .year)
        let classRoom = try container.decode(String?.self, forKey: .classRoom)
        let semester = try container.decode(String?.self, forKey: .semester)
        let teacher = try container.decode(String?.self, forKey: .teacher)
        self.init(id: id, sectionTimes: sectionTimes, subjectsName: subjectsName,
                  classRoom: classRoom, year: year, semester: semester, teacher: teacher)
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
}


class SectionTimeModel: Object, Decodable {
    @objc dynamic var id: Int = 0
    @objc dynamic var week = 0
    @objc dynamic var section = 0
    @objc dynamic var startTime = ""
    @objc dynamic var endTime = ""
    @objc dynamic var subjects = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case week = "week"
        case section = "section"
        case startTime = "start_time"
        case endTime = "end_time"
        case subjects = "subjects"
    }
    
    convenience init(id: Int, week: Int, section: Int,
                     startTime: String, endTime: String,
                     subjects: Int) {
        self.init()
        self.id = id
        self.week = week
        self.section = section
        self.startTime = startTime
        self.endTime = endTime
        self.subjects = subjects
    }
    
    convenience required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let id = try container.decode(Int.self, forKey: .id)
        let week = try container.decode(Int.self, forKey: .week)
        let section = try container.decode(Int.self, forKey: .section)
        let startTime = try container.decode(String.self, forKey: .startTime)
        let endTime = try container.decode(String.self, forKey: .endTime)
        let subjects = try container.decode(Int.self, forKey: .subjects)
        self.init(id: id, week: week, section: section,
                  startTime: startTime, endTime: endTime,
                  subjects: subjects)
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
}
