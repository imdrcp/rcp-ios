//
//  Theme.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/7/7.
//  Copyright © 2018年 gs1. All rights reserved.
//

import UIKit
import Hue


struct UITheme {
    
    static let startColor = UIColor(hex: "#f77062")
    static let endColor = UIColor(hex: "#fe5196")
    
    
    static func mainGradientLayer(frame rect: CGRect) -> CALayer {
        return [startColor, endColor].gradient { (gradient) -> CAGradientLayer in
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.frame = rect
            return gradient
        }
    }
}
