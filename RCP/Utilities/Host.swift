//
//  Host.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/5.
//  Copyright © 2018年 gs1. All rights reserved.
//

import Foundation

struct APIHost {
    static let product = "http://rcp.nctu.me:8000/api"
    static let development = "http://rcp.nctu.me:8000/api"
}
