//
//  Config.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/5.
//  Copyright © 2018年 gs1. All rights reserved.
//

import Foundation

struct Config {
    let  DEBUG: Bool
    let host: String
    let realmMigration:RealmMigration = RealmMigration()
    
    init(isDebug: Bool = false) {
        DEBUG = isDebug
        if isDebug {
            host = APIHost.development
            onDebugSetUp()
        }else {
            host = APIHost.product
            onProductSetUp()
        }
    }
    
    private
    func onDebugSetUp() {
        realmMigration.didApplicationLunch()
    }
    
    private
    func onProductSetUp() {
        realmMigration.didApplicationLunch()
    }
    
}
