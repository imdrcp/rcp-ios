//
//  UIView+Border.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/10/29.
//  Copyright © 2018 gs1. All rights reserved.
//

import UIKit

extension UIView {
    
    // Example use: myView.addBorder(toSide: .Left, withColor: UIColor.redColor().CGColor, andThickness: 1.0)
    
    enum ViewSide {
        case left, right, top, bottom
    }
    
    func addBorder(toSide side: ViewSide, withColor color: CGColor, andThickness thickness: CGFloat) {
        
        let border = getBorder(toSide: side, withColor: color, andThickness: thickness)
        
        layer.addSublayer(border)
    }
    
    func getBorder(toSide side: ViewSide, withColor color: CGColor, andThickness thickness: CGFloat)->CALayer{
        let border = CALayer()
        border.backgroundColor = color
        
        switch side {
        case .left: border.frame = CGRect(x: 0, y: 0, width: thickness, height: frame.height)
        case .right: border.frame = CGRect(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
        case .top: border.frame = CGRect(x: 0, y: 0, width: frame.width, height: thickness)
        case .bottom: border.frame = CGRect(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
        }
        return border
    }
}
