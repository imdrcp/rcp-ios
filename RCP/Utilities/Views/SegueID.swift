//
//  SegueID.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/4.
//  Copyright © 2018年 gs1. All rights reserved.
//

import Foundation

enum AuthSegueID: String {
    case setPassword = "Auth-SetPassword"
}
