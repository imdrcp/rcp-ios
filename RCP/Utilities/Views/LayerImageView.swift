//
//  LayerImageView.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/4.
//  Copyright © 2018年 gs1. All rights reserved.
//

import UIKit

@IBDesignable
class LayerImageView: UIImageView {
    @IBInspectable
    var radius: CGFloat {
        set {
            guard newValue >= 0.0 else {
                return
            }
            layer.masksToBounds = true
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
}
