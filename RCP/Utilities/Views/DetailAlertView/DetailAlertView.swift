//
//  DetailAlertView.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/11/2.
//  Copyright © 2018 gs1. All rights reserved.
//

import UIKit

class DetailAlertView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var contentStack: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var layerView: UIView!
    
    private var handle: (()->())? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupNib()
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupNib()
        initView()
    }
    
    
    private func setupNib() {
        let nib = UINib(nibName: "DetailAlertView", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    private func initView() {
        closeButton.addTarget(self, action: #selector(self.closeAction), for: .touchUpInside)
        layerView.layer.masksToBounds = true
        layerView.layer.cornerRadius = 17.0
        layerView.layer.borderColor = UIColor(hex: "#999999").cgColor
        layerView.layer.borderWidth = 0.5
        
        contentTextView.isScrollEnabled = false
        let fixedWidth = contentTextView.frame.size.width
        let newSize = contentTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        contentTextView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
    }

    @objc func closeAction() {
        closeView()
    }
    
    func show(innerView: UIView, handle: (()->())?) {
        self.handle = handle
        self.frame = innerView.frame
        innerView.addSubview(self)
        
    }
    
    func closeView() {
        if let doing = handle {
            doing()
        }
        
        self.removeFromSuperview()
    }
}
