//
//  LayerNavigationController.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/4.
//  Copyright © 2018年 gs1. All rights reserved.
//

import UIKit
import Hue

class LayerNavigationController: UINavigationController {


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpBarColor()
    }
    
    func setUpBarColor() {
        let colors = UITheme.mainGradientLayer(frame: self.navigationBar.frame)
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        if #available(iOS 11.0, *) {
            navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        }
        navigationBar.setBackgroundImage(UIImage.gradient(from: colors), for: .default)
        navigationBar.tintColor = .white
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
