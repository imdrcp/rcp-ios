//
//  LayerTabBarItem.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/5.
//  Copyright © 2018年 gs1. All rights reserved.
//

import UIKit
import Hue

@IBDesignable
class LayerTabBarItem: UITabBarItem {
    
    @IBInspectable
    var theBadgeColor: UIColor {
        get {
            return badgeColor ?? UIColor(hex: "#")
        }
        set {
            badgeColor = newValue
        }
    }
}
