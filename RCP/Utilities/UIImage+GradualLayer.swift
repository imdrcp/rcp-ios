//
//  UIImage+GradualLayer.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/7/7.
//  Copyright © 2018年 gs1. All rights reserved.
//

import UIKit


extension UIImage {
    static func gradient(from layer: CALayer) -> UIImage {
        UIGraphicsBeginImageContext(layer.frame.size)
        
        layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return outputImage!
    }
}
