//
//  XibPlusIn.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/11/2.
//  Copyright © 2018 gs1. All rights reserved.
//

import UIKit

protocol XibPlusIn {
    static var nibId: String {get}
    static var nib: UINib {get}
}

extension XibPlusIn {
    static var nib: UINib {
        return UINib(nibName: self.nibId, bundle: nil)
    }
}
