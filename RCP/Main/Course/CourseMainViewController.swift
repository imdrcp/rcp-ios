//
//  CourseMainViewController.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/7.
//  Copyright © 2018年 gs1. All rights reserved.
//

import UIKit
import PagingKit
import RxCocoa
import RxSwift
import CoreBluetooth
import CoreLocation

class CourseMainViewController: UIViewController, CommonViewController {

    var peripheralManager: CBPeripheralManager!
    var region: CLBeaconRegion!
    private let dbManger = RealmManager()

    var menuViewController: PagingMenuViewController!
    var contentViewController: PagingContentViewController!
    
    var dataSource = [CourseMenuModel]()
    private let viewModel = CourseMainViewModel()
    let disposeBag = DisposeBag()
    
    private var isTeacher: Bool = false {
        didSet {
            if isTeacher {
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "roll_call_black"),
                                                                         style: .done, target: self, action: #selector(setUpBluetooth))
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadDataSource()
    }
    
    func setUpView() {
        Authentication.instance.profile.teacher
            .asObservable()
            .map{$0 != nil}
            .subscribe(onNext: {[unowned self] in
                self.isTeacher = $0
            }).disposed(by: disposeBag)
        
        menuViewController.register(nib: CourseMenuCell.nib(), forCellWithReuseIdentifier: CourseMenuCell.id)
        menuViewController.registerFocusView(nib: UINib(nibName: "CourseMenuFocusView", bundle: nil))
    }
    
    func loadDataSource() {
        viewModel.loadSubjectsList().subscribe(onNext: {
            self.dataSource = $0
            self.reloadViews()
        }).disposed(by: disposeBag)
    }

    func reloadViews() {
        menuViewController.reloadData(with: 0, completionHandler: nil)
        contentViewController.reloadData(with: 0, completion: nil)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PagingMenuViewController {
            menuViewController = vc
            menuViewController.dataSource = self
            menuViewController.delegate = self
        } else if let vc = segue.destination as? PagingContentViewController {
            contentViewController = vc
            contentViewController.dataSource = self
            contentViewController.delegate = self
        }
    }

}


extension CourseMainViewController: PagingMenuViewControllerDataSource {
    func numberOfItemsForMenuViewController(viewController: PagingMenuViewController) -> Int {
        return dataSource.count
    }
    
    func menuViewController(viewController: PagingMenuViewController, widthForItemAt index: Int) -> CGFloat {
        return 100
    }
    
    func menuViewController(viewController: PagingMenuViewController, cellForItemAt index: Int) -> PagingMenuViewCell {
        let cell = viewController.dequeueReusableCell(withReuseIdentifier: CourseMenuCell.id, for: index) as! CourseMenuCell
        cell.title.text = dataSource[index].title
        return cell
    }
}

extension CourseMainViewController: PagingContentViewControllerDataSource {
    func numberOfItemsForContentViewController(viewController: PagingContentViewController) -> Int {
        return dataSource.count
    }
    
    func contentViewController(viewController: PagingContentViewController, viewControllerAt index: Int) -> UIViewController {
        let vc = dataSource[index].vc
        vc.models = dataSource[index].dataSource
        return vc
    }
}

extension CourseMainViewController: PagingMenuViewControllerDelegate {
    func menuViewController(viewController: PagingMenuViewController, didSelect page: Int, previousPage: Int) {
        contentViewController.scroll(to: page, animated: true)
    }
}

extension CourseMainViewController: PagingContentViewControllerDelegate {
    func contentViewController(viewController: PagingContentViewController, didManualScrollOn index: Int, percent: CGFloat) {
        menuViewController.scroll(index: index, percent: percent, animated: false)
    }
}

extension CourseMainViewController: CBPeripheralManagerDelegate {
    @objc func setUpBluetooth() {
        let queue = DispatchQueue.global()
        
        guard let classInfo = getSectionTime(),
              let beacon = getBeacon(from: classInfo)
        else {
            return
        }
        
        peripheralManager = CBPeripheralManager(delegate: self, queue: queue)
        region = CLBeaconRegion(
            proximityUUID: UUID(uuidString: beacon.beaconUUID)!,
            major: CLBeaconMajorValue(beacon.beaconMajor),
            minor: CLBeaconMinorValue(beacon.beaconMinor),
            identifier: beacon.beaconId
        )
    }
    
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        guard peripheral.state == .poweredOn else {
            beaconAlert(type: "bluetoothAlert", from: nil)
            return
        }
        
        peripheral.delegate = self
        
        var advData = [String: Any]()
        for (key, value) in region.peripheralData(withMeasuredPower: nil) {
            advData[key as! String] = value
        }
        
        advData[CBAdvertisementDataLocalNameKey] = "myBeacon"
        peripheral.startAdvertising(advData)
        
        DispatchQueue.main.async {
            self.beaconAlert(type: "beaconAlert", from: peripheral)
        }
    }
    
    private func getBeacon(from classInfo: SectionTimeModel) -> BeaconModel? {
        let predicate = NSPredicate(format: "curriculum = \(classInfo.subjects)")
        let querySet = self.dbManger.read(model: BeaconModel.self, condition: predicate)
        guard let query = querySet else {
            return nil
        }
        
        return (query.last as? BeaconModel)
    }
    
    private func getSectionTime() -> SectionTimeModel? {
        var classInfo = SectionTimeModel()
        let querySet = self.dbManger.readAll(model: SectionTimeModel.self)
        guard let query = querySet else {
            return nil
        }
        
        for item in query {
            let subject = item as! SectionTimeModel
            if let subject = validClassTime(from: subject) {
                classInfo = subject
            } else {
                self.beaconAlert(type: "classAlert", from: nil)
            }
        }
        
        return classInfo
    }
    
    private func validClassTime(from subject: SectionTimeModel) -> SectionTimeModel? {
        let date = Date()
        let calendar = Calendar.current
        let nowWeek = calendar.component(.weekday, from: date)
        let nowHour = calendar.component(.hour, from: date)
        let nowMinute = calendar.component(.minute, from: date)
        
        let classStartHour = subject.startTime.getHour()
        let classStartMinute = subject.startTime.getMinute()
        let classEndHour = subject.endTime.getHour()
        let classEndMinute = subject.endTime.getMinute()
        
        var validClassTime: Bool {
            if nowHour == classStartHour {
                if nowMinute >= classStartMinute {
                    return true
                }
            } else if nowHour == classEndHour {
                if nowMinute <= classEndMinute {
                    return true
                }
            }
            return false
        }
        
        return nowWeek == subject.week && validClassTime ? subject : nil
    }
    
    private func beaconAlert(type: String, from peripheral: CBPeripheralManager?) {
        let alertController = UIAlertController(title: "點名Beacon", message: nil, preferredStyle: .alert)
        
        switch type {
        case "beaconAlert":
            let continueAction = UIAlertAction(title: "持續發送", style: .default, handler: nil)
            alertController.addAction(continueAction)
            
            let stopAction = UIAlertAction(title: "停止發送", style: .destructive) { _ in
                peripheral!.stopAdvertising()
            }
            alertController.addAction(stopAction)
        case "bluetoothAlert":
            alertController.message = "藍芽尚未開啟"
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
        case "classAlert":
            alertController.message = "非上課時段"
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
        default:
            break
        }

        if presentedViewController == nil {
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

extension String {
    func getHour() -> Int {
        let hourStr = self.split(separator: ":").first!
        return Int(hourStr)!
    }
    
    func getMinute() -> Int {
        let minuteStr = self.split(separator: ":").last!
        return Int(minuteStr)!
    }
}
