//
//  CourseMenuContentTableViewCell.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/7.
//  Copyright © 2018年 gs1. All rights reserved.
//

import UIKit

class CourseMenuContentTableViewCell: UITableViewCell {

    @IBOutlet weak var section: UILabel!
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var endTime: UILabel!
    @IBOutlet weak var teacher: UILabel!
    @IBOutlet weak var classRoom: UILabel!
    @IBOutlet weak var subjectsName: UILabel!
    @IBOutlet weak var hdash: UILabel!
    @IBOutlet weak var sectionBackground: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        sectionBackground.image = UIImage.gradient(from: UITheme.mainGradientLayer(frame: section.frame))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static let nib = UINib(nibName: "CourseMenuContentTableViewCell", bundle: nil)
    static let id = "CourseMenuContentTableViewCell"
    
}
