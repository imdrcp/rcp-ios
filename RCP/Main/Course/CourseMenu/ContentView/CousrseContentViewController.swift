//
//  CousrseContentViewController.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/7.
//  Copyright © 2018年 gs1. All rights reserved.
//

import UIKit

class CousrseContentViewController: UIViewController, CommonViewController {
    
    
    var dataSource = Array(repeating: SubjectSectionModel(), count: 16)
    var models = [SubjectModel]()
    private let sectionText = ["", "第 1 節", "第 2 節", "第 3 節", "第 4 節", "第 5 節", "第 6 節",
                               "第 7 節", "第 8 節", "第 9 節","第 10 節", "第 11 節", "第 12 節", "第 13 節",
                               "第 14 節", "第 15 節"]

    @IBOutlet weak var tableView: UITableView!
    
    
    static let nibName = "CousrseContentViewController"
    static var makeViewController: ()-> CousrseContentViewController = {
        return CousrseContentViewController(nibName: CousrseContentViewController.nibName, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        resetDataSource()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpView() {
        tableView.register(CourseMenuContentTableViewCell.nib,
                           forCellReuseIdentifier: CourseMenuContentTableViewCell.id)
    }
    
    func resetDataSource() {
        guard models.count > 0 else {
            return
        }
        
        models.forEach { (m) in
            if !m.sectionTimes.isInvalidated {
                m.sectionTimes.forEach({ (n) in
                    self.dataSource[n.section] = SubjectSectionModel(section: self.sectionText[n.section],
                                                                     startTime: n.startTime, endTime: n.endTime, teacher: m.teacher ?? "",
                                                                     classRoom: m.classRoom ?? "", subjectsName: m.subjectsName)
                })
            }
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CousrseContentViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataSource.count - 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CourseMenuContentTableViewCell.id,
                                                 for: indexPath) as! CourseMenuContentTableViewCell
        
        let d = dataSource[indexPath.row + 1]
        cell.section.text = self.sectionText[indexPath.row + 1]
        cell.classRoom.text = d.classRoom
        cell.startTime.text = d.startTime
        cell.teacher.text = d.teacher
        cell.endTime.text = d.endTime
        cell.subjectsName.text = d.subjectsName
        cell.hdash.text = d.hasShowDash()
        
        return cell
    }
}
