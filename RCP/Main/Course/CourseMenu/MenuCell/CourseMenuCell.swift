//
//  CourseMenuCell.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/7.
//  Copyright © 2018年 gs1. All rights reserved.
//

import UIKit
import PagingKit

class CourseMenuCell: PagingMenuViewCell {

    @IBOutlet weak var title: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    static let id = "CourseMenuCell"
    
    static func nib()-> UINib {
        return UINib(nibName: CourseMenuCell.id, bundle: nil)
    }

}
