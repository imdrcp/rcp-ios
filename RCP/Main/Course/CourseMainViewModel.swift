//
//  CourseMainViewModel.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/7.
//  Copyright © 2018年 gs1. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import Alamofire
import Hydra

class CourseMainViewModel: CommonViewModel {
    
    let dataSource = Variable<[CourseMenuModel]>([])
    
    private var apiProvider: MoyaProvider<SubjectsAPI>!
    private let auth = Authentication.instance
    private let weekText = ["", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"]
    
    init() {
        
    }
    
    func loadSubjectsList()-> Observable<[CourseMenuModel]> {
//        let db = RealmManager()
        defer {
            auth.refresh()
        }
        if auth.isAuth {
            apiProvider = MoyaProvider<SubjectsAPI>()
            return apiProvider.rx.request(.getSubjectsList).map([SubjectModel].self)
                .map({ (subjects) -> [CourseMenuModel] in
                    return self.collectCourses(subjects)
                }).asObservable()
        }
        
        let models = self.getDBSubjects()
        return Observable<[CourseMenuModel]>.create({ (observer) -> Disposable in
            let course = self.collectCourses(models)
            observer.onNext(course)
            observer.onCompleted()
            
            return Disposables.create()
        })
    }
    
    private func getDBSubjects()->[SubjectModel] {
        let db = RealmManager()
        let subjects = db.readAll(model: SubjectModel.self)
        
        guard let sub = subjects else {
            return []
        }
        
        return sub.map({ (object) -> SubjectModel in
            return object as! SubjectModel
        })
    }
    
    private func collectCourses(_ subjects: [SubjectModel])-> [CourseMenuModel] {
        
        guard subjects.count > 0 else {
            return []
        }
        
        var courses = [CourseMenuModel]()
        for i in 1...7 {
            let sub = subjects.filter({ (subject) -> Bool in
                if let week = subject.sectionTimes.first?.week {
                    return week == i
                }
                return false
            })
            let course = CourseMenuModel(title: self.weekText[i], vc: CousrseContentViewController.makeViewController(), dataSource: sub)
            courses.append(course)
        }
        return courses
    }
}
