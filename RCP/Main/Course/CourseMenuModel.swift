//
//  CourseMenuModel.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/7.
//  Copyright © 2018年 gs1. All rights reserved.
//

import Foundation
import UIKit


struct CourseMenuModel {
    let title: String
    let vc: CousrseContentViewController
    let dataSource: [SubjectModel]
}


struct SubjectSectionModel {
    var section: String = ""
    var startTime: String = ""
    var endTime: String = ""
    var teacher: String = ""
    var classRoom: String = ""
    var subjectsName: String = ""
    func subjectsNameBackgroundColor() ->UIColor {
        if self.subjectsName.isEmpty {
            return UIColor.white
        }
        return UIColor(hex: "#FFC6C2".lowercased())
    }
    
    func hasShowDash()-> String {
        if self.subjectsName.isEmpty {
            return ""
        }
        return "~"
    }
    
    init() {
    }
    
    init(section: String, startTime: String, endTime: String, teacher: String, classRoom: String, subjectsName: String) {
        self.section = section
        self.startTime = startTime
        self.endTime = endTime
        self.teacher = teacher
        self.classRoom = classRoom
        self.subjectsName = subjectsName
    }
    
}
