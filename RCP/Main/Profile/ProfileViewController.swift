//
//  ProfileViewController.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/10/23.
//  Copyright © 2018 gs1. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var labelsStack: UIStackView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var studentIdLabel: UILabel!
    @IBOutlet weak var eduTypeLabel: UILabel!
    @IBOutlet weak var departmentLabel: UILabel!
    @IBOutlet weak var accountLabel: UILabel!
    
    private var disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        steupView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setupProfile()
    }
    
    private func steupView() {
        avatarImageView.backgroundColor = UIColor.white
        avatarImageView.layer.borderColor = UIColor(hex: "#999999").cgColor
        avatarImageView.layer.borderWidth = 1.0
        avatarImageView.layer.masksToBounds = true
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width / 2
    }
    
    private func setupProfile() {
        let profile = Authentication.instance.profile
        
        profile.student
            .asObservable()
            .filter{$0 != nil}
            .map{$0!}
            .subscribe(onNext: {[unowned self] student in
                self.nameLabel.text = "姓名：\(student.user.firstName ?? "")\(student.user.lastName ?? "")"
                self.accountLabel.text = "名稱：\(student.user.userName)"
                self.studentIdLabel.text = "學號：\(student.idNumber)"
                self.eduTypeLabel.text = "學制：\(student.eduType)"
                self.departmentLabel.text = "科系：\(student.department)"
                self.addBottomBorder() 
            }).disposed(by: disposeBag)
        
        profile.teacher
            .asObservable()
            .filter{$0 != nil}
            .map{$0!}
            .subscribe(onNext: {[unowned self] teacher in
                self.nameLabel.text = "姓名：\(teacher.user.lastName ?? "")\(teacher.user.firstName ?? "")"
                self.accountLabel.text = "名稱：\(teacher.user.userName)"
                self.studentIdLabel.text = "職稱：\(teacher.jobTitle)"
                self.eduTypeLabel.text = "專兼任：\(teacher.timeCase)"
                self.departmentLabel.text = "科系：\(teacher.department)"
                self.addBottomBorder()
            }).disposed(by: disposeBag)
        
    }
    
    private func addBottomBorder() {
        labelsStack.addBorder(toSide: .bottom, withColor: UIColor(hex: "#999999").cgColor, andThickness: 1.0)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
