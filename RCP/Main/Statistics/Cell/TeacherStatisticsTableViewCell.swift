//
//  TeacherStatisticsTableViewCell.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/11/2.
//  Copyright © 2018 gs1. All rights reserved.
//

import UIKit

class TeacherStatisticsTableViewCell: UITableViewCell {

    @IBOutlet weak var labelStack: UIStackView!
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension TeacherStatisticsTableViewCell: XibPlusIn {
    static let nibId = "TeacherStatisticsTableViewCell"
}
