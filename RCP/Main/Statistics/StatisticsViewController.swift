//
//  StatisticsViewController.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/10/30.
//  Copyright © 2018 gs1. All rights reserved.
//

import UIKit
import Charts
import Moya
import RxSwift
import RxCocoa

class StatisticsViewController: UIViewController {
    @IBOutlet weak var courseTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textFieldView: UIView!
    
    private var fullModels = [RecordModel]()
    private var models = [RecordModel]() {
        didSet {
            groupByModels = Dictionary(grouping: models, by: {$0.recordType})
        }
    }
    private var groupByModels = [String: [RecordModel]]() {
        didSet {
            DispatchQueue.main.async {
                self.showPieData()
            }
            dataSource = groupByModels.map{_, mds in mds}
        }
    }
    private var dataSource = [[RecordModel]]() {
        didSet {
            tableView.reloadData()
        }
    }
    private var isTeacher: Bool = false {
        didSet {
            textFieldView.isHidden = !isTeacher
        }
    }
    private var classArray = Array<Int>()
    private var dateArray = Array<String>()
    private var apiService = MoyaProvider<RollCallAPI>()
    private var dbManager = RealmManager()
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.title = "統計"
        pieChartView.noDataText = "資料讀取中"
        setupPie()
        setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        loadData()
    }
    
    private func setupPie() {
        pieChartView.usePercentValuesEnabled = true
        pieChartView.drawSlicesUnderHoleEnabled = false
        pieChartView.holeRadiusPercent = 0.58
        pieChartView.transparentCircleRadiusPercent = 0.61
        pieChartView.chartDescription?.enabled = false
        pieChartView.setExtraOffsets(left: 0, top: 0, right: 0, bottom: 0)
        
        pieChartView.drawCenterTextEnabled = true
        
        pieChartView.centerText = "缺曠紀錄"
        
        pieChartView.drawHoleEnabled = true
        pieChartView.rotationAngle = 0
        pieChartView.rotationEnabled = true
        pieChartView.highlightPerTapEnabled = true
        
        let legend = pieChartView.legend
        legend.horizontalAlignment = .right
        legend.verticalAlignment = .top
        legend.orientation = .vertical
        legend.xEntrySpace = 4
        legend.yEntrySpace = 0
        legend.yOffset = 0
        
        pieChartView.delegate = self
        
        // entry label styling
        pieChartView.entryLabelColor = .black
        pieChartView.entryLabelFont = .systemFont(ofSize: 12, weight: .light)
    }
    
    private func setupPickerView() {
        classArray = models.compactMap { $0.sectionTime.subjects }
        classArray = Array(Set(classArray))
        dateArray = models.compactMap { $0.recordDate }
        dateArray = Array(Set(dateArray))
        
        let coursePickerView = UIPickerView()
        coursePickerView.delegate = self
        coursePickerView.tag = 0
        courseTextField.inputView = coursePickerView
        
        let datePickerView = UIPickerView()
        datePickerView.delegate = self
        datePickerView.tag = 1
        dateTextField.inputView = datePickerView
    }
    
    private func loadData() {
        apiService.rx.request(.records)
            .retry(3)
            .timeout(30, scheduler: MainScheduler.instance)
            .filterSuccessfulStatusCodes()
            .map([RecordModel].self)
            .asObservable()
            .catchErrorJustReturn([])
            //.map{Dictionary.init(grouping: $0, by: {$0.recordType})}
            .subscribe(onNext: {[unowned self] in
                guard $0.count > 0 else{
                    self.pieChartView.noDataText = "暫無缺曠紀錄"
                    return
                }
                self.models = $0
                self.fullModels = $0
                
                if self.isTeacher {
                    self.setupPickerView()
                }
            }).disposed(by: disposeBag)
        
    }
    
    private func showPieData() {
        let entries = groupByModels.map{[unowned self] key, data -> PieChartDataEntry in
            let vl = Double(Float(data.count)/Float(self.models.count))
            return PieChartDataEntry(value: vl, label: key)
        }
        let set = PieChartDataSet(values: entries, label: "")
        set.drawIconsEnabled = false
        //set.sliceSpace = 2
        set.colors = ChartColorTemplates.vordiplom()
            + ChartColorTemplates.joyful()
            + ChartColorTemplates.colorful()
            + ChartColorTemplates.liberty()
            + ChartColorTemplates.pastel()
            + [UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1)]
        let pieData = PieChartData(dataSet: set)
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = " %"
        pieData.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
        
        pieData.setValueFont(.systemFont(ofSize: 10, weight: .light))
        pieData.setValueTextColor(.black)
        pieChartView.data = pieData
        pieChartView.highlightValues(nil)
        pieChartView.animate(xAxisDuration: 1.4, yAxisDuration: 1.4)
        pieChartView.setNeedsDisplay()
    }
    
    
    private func setupTableView() {
        Authentication.instance.profile.teacher
            .asObservable()
            .map{$0 != nil}
            .subscribe(onNext: {[unowned self] in
                self.isTeacher = $0
            }).disposed(by: disposeBag)
        
        tableView.register(TeacherStatisticsTableViewCell.nib,
                           forCellReuseIdentifier: TeacherStatisticsTableViewCell.nibId)
        tableView.delegate = self
        tableView.dataSource = self
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension StatisticsViewController: ChartViewDelegate {
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        NSLog("chartValueSelected");
    }
    
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        NSLog("chartValueNothingSelected");
    }
    
    func chartScaled(_ chartView: ChartViewBase, scaleX: CGFloat, scaleY: CGFloat) {
        
    }
    
    func chartTranslated(_ chartView: ChartViewBase, dX: CGFloat, dY: CGFloat) {
        
    }
}

extension StatisticsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TeacherStatisticsTableViewCell.nibId, for: indexPath) as! TeacherStatisticsTableViewCell
        let data = dataSource[indexPath.section][indexPath.row]
        cell.subjectLabel.text = getCurriculum(from: data.sectionTime.subjects)?.subjectsName ?? ""
        cell.nameLabel.text = data.student.user.fullname()
        cell.dateLabel.text = data.recordDate
        cell.nameLabel.isHidden = !isTeacher
        return cell
    }
    
    private func getCurriculum(from sectionTimeId: Int) -> SubjectModel? {
        let predicate = NSPredicate(format: "id = \(sectionTimeId)")
        let querySet = self.dbManager.read(model: SubjectModel.self, condition: predicate)
        guard let query = querySet else {
            return nil
        }
        return (query.last as? SubjectModel)
    }
    
}

extension StatisticsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let data = dataSource[section].first!
        return data.recordType
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}

extension StatisticsViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return classArray.isEmpty ? 0 : classArray.count
        case 1:
            return dateArray.isEmpty ? 0 : dateArray.count
        default:
            return 0
        }
    }
}

extension StatisticsViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return getCurriculum(from: classArray[row])?.subjectsName ?? ""
        case 1:
            return dateArray[row].description
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 0:
            if !classArray.isEmpty {
                courseTextField.text =  getCurriculum(from: classArray[row])?.subjectsName ?? ""
            }
        case 1:
            if !dateArray.isEmpty {
                dateTextField.text = dateArray[row].description
            }
        default:
            break
        }
        
        self.models = self.fullModels
        if !self.courseTextField.text!.isEmpty && !self.courseTextField.text!.isEmpty {
            self.models = self.models.filter {
                $0.sectionTime.subjects == self.classArray[row] && $0.recordDate == self.dateArray[row]
            }
        } else if !self.courseTextField.text!.isEmpty {
            self.models = self.models.filter { $0.sectionTime.subjects == self.classArray[row] }
        } else if !self.courseTextField.text!.isEmpty {
            self.models = self.models.filter { $0.recordDate == self.dateArray[row] }
        }
    }
}
