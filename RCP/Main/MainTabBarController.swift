//
//  MainTabBarController.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/6.
//  Copyright © 2018年 gs1. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Moya
import CoreBluetooth
import CoreLocation
import UserNotifications

class MainTabBarController: LayerTabBarController {

    let locationManager = CLLocationManager()
    
    private let disposeBag = DisposeBag()
    private let subjectsApi = MoyaProvider<SubjectsAPI>()
    private let beaconApi = MoyaProvider<RollCallAPI>()
    private let dbManger = RealmManager()
    private var student: StudentModel? = nil {
        didSet {
            loadBeacons()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadSubjects()
        loadBeacons()
        Authentication.instance.profile.student
            .asObservable()
            .filter{$0 != nil}
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: {[unowned self] in
                self.student = $0
            }).disposed(by: disposeBag)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func loadSubjects() {
        subjectsApi.rx
            .request(.getSubjectsList)
            .retry(3)
            .timeout(30, scheduler: MainScheduler.instance)
            .filterSuccessfulStatusCodes()
            .map([SubjectModel].self)
            .catchErrorJustReturn([])
            .subscribeOn(MainScheduler.asyncInstance)
            .subscribe(onSuccess: {[unowned self] models in
                guard models.count > 0 else {
                    return
                }
                for md in models {
                    _ = self.dbManger.createOrUpdate(model: md)
                }
                NSLog("loadSubjects finished.")
            }).disposed(by: disposeBag)
    }
    
    
    private func loadBeacons() {
        beaconApi.rx.request(.beacons)
            .retry(3)
            .timeout(30, scheduler: MainScheduler.instance)
            .filterSuccessfulStatusCodes()
            .map([BeaconModel].self)
            .catchErrorJustReturn([])
            .subscribeOn(MainScheduler.asyncInstance)
            .subscribe(onSuccess: {[unowned self] models in
                guard models.count > 0 else {
                    return
                }
                for md in models {
                    _ = self.dbManger.createOrUpdate(model: md)
                }
                if self.student != nil {
                    self.setLocationManager(beacons: models)
                }
                NSLog("loadBeacons finished.")
            }).disposed(by: disposeBag)
    }
    
    private func message(_ text: String){
        DispatchQueue.global().async {
            let content = UNMutableNotificationContent()
            content.title = "RCP 點名成功"
            content.body = text
            
            content.sound = UNNotificationSound.default
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
            let request = UNNotificationRequest(identifier: "beacon", content: content, trigger: trigger)
            
            let center = UNUserNotificationCenter.current()
            center.add(request, withCompletionHandler: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MainTabBarController: CLLocationManagerDelegate {
    
    private func setLocationManager(beacons: [BeaconModel]) {
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.delegate = self
        
        for beacon in beacons {
            let region = CLBeaconRegion(
                proximityUUID: UUID(uuidString: beacon.beaconUUID)!,
                identifier: beacon.beaconId)
            self.locationManager.startRangingBeacons(in: region)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        //dismissExhibit(major: major, minor: minor)
        
        for beacon in beacons {
//            print(beacon.proximityUUID)
//            switch beacon.proximity {
//            case .immediate:
//                print("Beacon is immediate.")
//            case .near:
//                print("Beacon is near.")
//            case .far:
//                print("Beacon is far.")
//            case .unknown:
//                print("Beacon is unknown.")
//            }
            
            guard let beacon = getBeacon(from: beacon),
                let sectionTime = getSectionTime(from: beacon.curriculum),
                let stud = student
            else {
                return
            }
            
            if !self.checkRollCallTime(beacon: beacon, sectionTime: sectionTime) {
                return
            }

            beaconApi.rx.request(
                    .createCheck(student: stud.user.id, beacon: beacon.id, sectionTime: sectionTime.id)
                ).retry(3)
                .filterSuccessfulStatusCodes()
                .mapString()
                .subscribeOn(MainScheduler.asyncInstance)
                .catchErrorJustReturn("")
                .filter{!$0.isEmpty}
                .subscribe(onSuccess: {[unowned self]_ in
                    self.addRollCallTime(beacon: beacon, sectionTime: sectionTime)
                    let classname = beacon.beaconId.split(separator: "的").first ?? ""
                    self.message("\(classname)")
//                    self.locationManager.stopRangingBeacons(in: region)
                }).disposed(by: disposeBag)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
        bluetoothAlert()
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print(error.localizedDescription)
        bluetoothAlert()
    }
    
    func locationManager(_ manager: CLLocationManager, rangingBeaconsDidFailFor region: CLBeaconRegion, withError error: Error) {
        print(error.localizedDescription)
        bluetoothAlert()
    }

    private func getBeacon(from region: CLBeacon) -> BeaconModel? {
        let uuid = region.proximityUUID.uuidString.lowercased()
        let major = region.major
        let minor = region.minor
        let predicate = NSPredicate(format: "beaconUUID = %@ AND beaconMajor = %@ AND beaconMinor = %@", uuid, major, minor)
        let querySet = self.dbManger.read(model: BeaconModel.self, condition: predicate)
        guard let query = querySet else {
            return nil
        }
        return (query.last as? BeaconModel)
    }

    private func getSectionTime(from curriculum: Int) -> SectionTimeModel? {
        let predicate = NSPredicate(format: "subjects = \(curriculum)")
        let querySet = self.dbManger.read(model: SectionTimeModel.self, condition: predicate)
        guard let query = querySet else {
            return nil
        }
        return (query.last as? SectionTimeModel)
    }
    
    func addRollCallTime(beacon: BeaconModel, sectionTime: SectionTimeModel) {
        let dateFormatter = DateFormatter.setTimeZone()
        let rollCallTime = dateFormatter.string(from: Date())
        let model = BeaconRecordModel(beacon: beacon, sectionTime: sectionTime, rollCallTime: rollCallTime)
        _ = self.dbManger.createOrUpdate(model: model)
    }
    
    func checkRollCallTime(beacon: BeaconModel, sectionTime: SectionTimeModel) -> Bool {
        let dateFormatter = DateFormatter.setTimeZone()
        let predicate = NSPredicate(format: "beacon = %@ AND sectionTime = %@", beacon, sectionTime)
        let querySet = self.dbManger.read(model: BeaconRecordModel.self, condition: predicate)
        guard let query = querySet?.last as? BeaconRecordModel else {
            return true
        }
        
        let nowString = dateFormatter.string(from: Date())
        let nowDate = dateFormatter.date(from: nowString)!
        let pastDate = dateFormatter.date(from: query.rollCallTime)!
        
        let isRollCall = nowDate >= pastDate + 300 ? true : false
        return isRollCall
    }
    
    private func bluetoothAlert() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        alertController.title = "接受點名Beacon失敗"
        alertController.message = "請確認「定位」及「藍芽」已允許及開啟"
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        
        if presentedViewController == nil {
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

extension TimeZone {
    static let taipei = TimeZone(secondsFromGMT: +28800)
}

extension DateFormatter {
    static func setTimeZone() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.taipei
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW")
        
        return dateFormatter
    }
}
