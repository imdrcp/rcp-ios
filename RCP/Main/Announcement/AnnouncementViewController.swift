//
//  AnnouncementViewController.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/11/2.
//  Copyright © 2018 gs1. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import RxCocoa

class AnnouncementViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private var dataSource: [AnnouncementModel] = [AnnouncementModel]() {
        didSet {
            tableView.reloadData()
        }
    }
    private let apiService = MoyaProvider<AnnouncementAPI>()
    private let disposeBag = DisposeBag()
    lazy var alertView = DetailAlertView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.layer.masksToBounds = true
        tableView.layer.cornerRadius = 17.0
        tableView.layer.borderColor = UIColor(hex: "#999999").cgColor
        tableView.layer.borderWidth = 1.0
        tableView.register(AnnouncementTableViewCell.nib,
                           forCellReuseIdentifier: AnnouncementTableViewCell.nibId)
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        apiService.rx.request(.posts)
            .retry(3)
            .timeout(30, scheduler: MainScheduler.instance)
            .filterSuccessfulStatusCodes()
            .map([AnnouncementModel].self)
            .asObservable()
            .catchErrorJustReturn([])
            .subscribe(onNext: {[unowned self] datas in
                self.dataSource = datas
            }).disposed(by: disposeBag)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AnnouncementViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AnnouncementTableViewCell.nibId, for: indexPath) as! AnnouncementTableViewCell
        let data = dataSource[indexPath.row]
        cell.titleLabel.text = data.title
        cell.dateLabel.text = String(data.postDate.split(separator: "T").first!)
        return cell
    }
    
    
}

extension AnnouncementViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.allowsSelection = false
        let data = dataSource[indexPath.row]
        alertView.titleLabel.text = data.title
        alertView.contentTextView.text = data.content
        alertView.show(innerView: self.view, handle: {
            self.tableView.allowsSelection = true
        })
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
