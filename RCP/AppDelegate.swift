//
//  AppDelegate.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/5/28.
//  Copyright © 2018年 gs1. All rights reserved.
//

import UIKit
import Hydra
import UserNotifications
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var config: Config?
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current()
            .requestAuthorization(
                options: [.alert, .sound],
                completionHandler: { (granted, error) in
                    if !granted {
                        NSLog("Not allow")
                    }
                })
        
        self.config = Config(isDebug: true)
        let auth = Authentication.instance
        auth.getVerify().then(in: .main) { (isVerify) in
            self.window = UIWindow(frame: UIScreen.main.bounds)
            var rootViewController: UIViewController!
            if isVerify {
                if !auth.isAuth {
                    auth.refresh()
                }
                let sb = UIStoryboard(name: "Main", bundle: nil)
                rootViewController = sb.instantiateViewController(withIdentifier: "main-tabBarController")
            }else{
                let sb = UIStoryboard(name: "Auth", bundle: nil)
                rootViewController = sb.instantiateViewController(withIdentifier: "auth-loginController")
            }
            DispatchQueue.main.async {
                auth.loadUserInfo()
            }
            self.window?.rootViewController = rootViewController
            self.window?.makeKeyAndVisible()
        }

        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
}
