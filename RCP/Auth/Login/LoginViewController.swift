//
//  LoginViewController.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/4.
//  Copyright © 2018年 gs1. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Hue
import KRProgressHUD

class LoginViewController: UIViewController, CommonViewController {
    let viewModel = LoginViewModel()
    
    
    @IBOutlet weak var username: LayerTextField!
    @IBOutlet weak var password: LayerTextField!
    
    @IBOutlet weak var loginButton: LayerButton!
    @IBOutlet weak var forgetPasswordButton: UIButton!
    var usernameVerify: Observable<String>!
    var passwordVerify: Observable<String>!
    
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        forgetPasswordButton.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func forgetPassword(_ sender: UIButton) {
    }
    
    @IBAction func signIn(_ sender: LayerButton) {
        KRProgressHUD.set(style: .custom(background: UIColor(hex: "#FFC6C2".lowercased()), text: UIColor.white, icon: nil)).set(maskType: .white).show()
        self.viewModel.login().subscribe(onNext: { (isLogin) in
            self.toMainStoryBoard(isLogin: isLogin)
        }).disposed(by: self.disposeBag)
        DispatchQueue.main.asyncAfter(deadline: .now()+3) {
            KRProgressHUD.dismiss()
        }
    }
    
    func setUpView() {
        usernameVerify = username.rx.text.orEmpty.asObservable()
        passwordVerify = password.rx.text.orEmpty.asObservable()
        
        username.rx.controlEvent([.editingDidEnd, .editingChanged]).asObservable().subscribe(onNext:{
            self.usernameVerify.subscribe(onNext:{
                if RegexVerification(".+@.+.[.].+.[.].+").match(input: $0) {
                    self.username.borderDefault()
                    self.viewModel.username.value = $0
                }else{
                    self.username.borderFail()
                }
                
            }).disposed(by: self.disposeBag)
        }).disposed(by: disposeBag)
        
        password.rx.controlEvent([.editingDidEnd, .editingChanged]).asObservable().subscribe(onNext:{
            self.passwordVerify.subscribe(onNext:{
                if $0.count >= 6 {
                    self.password.borderDefault()
                    self.viewModel.password.value = $0
                }else{
                    self.password.borderFail()
                }
                
            }).disposed(by: self.disposeBag)
        }).disposed(by: disposeBag)
        
        Observable.combineLatest(viewModel.username.asObservable(), viewModel.password.asObservable()){
            RegexVerification(".+@.+..+..+").match(input: $0) && $1.count >= 6
            }.bind(to: loginButton.rx.isEnabled).disposed(by: disposeBag)
    }
    
    
    func toMainStoryBoard(isLogin: Bool) {
        
        guard isLogin else {
            return
        }
        
        if isLogin {
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let viewController = sb.instantiateViewController(withIdentifier: "main-tabBarController")
            self.present(viewController, animated: false, completion: {
                KRProgressHUD.showSuccess()
                DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                    KRProgressHUD.dismiss()
                }
            })
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

fileprivate
extension LayerTextField {
    func borderFail() {
        backgroundColor = UIColor(hex: "#FFC6C2".lowercased())
    }
    
    func borderDefault() {
        backgroundColor = UIColor.white
    }
}
