//
//  LoginViewModel.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/6.
//  Copyright © 2018年 gs1. All rights reserved.
//

import Foundation
import RxSwift


struct LoginViewModel: CommonViewModel {
    let username = Variable<String>("")
    let password = Variable<String>("")
    
    init() {
        
    }
    
    func login()-> Observable<Bool> {
        return Observable<Bool>.create({ (observer) -> Disposable in
            let auth = Authentication.instance
            auth.login(username: self.username.value, password: self.password.value)
            print(auth.isAuth)
            observer.onNext(auth.isAuth)
            observer.onCompleted()
            return Disposables.create()
        }).retry(3)
    }
}
