//
//  AccountModel.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/10/29.
//  Copyright © 2018 gs1. All rights reserved.
//

import Foundation


struct OrgModel: Codable {
    let orgCode: String
    let orgType: String
    let country: String
    let abbreviation: String
    let name: String
    
    private enum CodingKeys: String, CodingKey {
        case orgCode = "org_code"
        case orgType = "org_type"
        case country
        case abbreviation
        case name
    }
}

struct AccountModel: Codable {
    let id: Int
    let userName: String
    let firstName: String?
    let lastName: String?
    let email: String
    let isActive: Bool
    
    private enum CodingKeys: String, CodingKey {
        case id
        case userName = "username"
        case firstName = "first_name"
        case lastName = "last_name"
        case email
        case isActive = "is_active"
    }
    
    func save() {
        let jsonEncode = try? JSONEncoder().encode(self)
        if let data = jsonEncode, let json = String(data: data, encoding: .utf8) {
            UserDefaults.standard.set(json, forKey: "AccountModel")
        }
    }
    
    static func restore() -> AccountModel?{
        if let json = UserDefaults.standard.object(forKey: "AccountModel") as? String {
            let jsonDecode = try! JSONDecoder().decode(AccountModel.self, from: json.data(using: .utf8)!)
            return jsonDecode
        }
        return nil
    }
    
    func fullname() -> String {
        return "\(self.lastName ?? "")\(self.firstName ?? "")"
    }
}

struct StudentModel: Codable {
    let user: AccountModel
    let unitCol: OrgModel
    let department: String
    let eduType: String
    let eduYear: String
    let teamType: String
    let idNumber: String
    let classNumber: String
    
    private enum CodingKeys: String, CodingKey {
        case user
        case unitCol = "unit_col"
        case department
        case eduType = "edu_type"
        case eduYear = "edu_year"
        case teamType = "team_type"
        case idNumber = "id_number"
        case classNumber = "class_number"
    }
    
    
    func save() {
        let jsonEncode = try? JSONEncoder().encode(self)
        if let data = jsonEncode, let json = String(data: data, encoding: .utf8) {
            UserDefaults.standard.set(json, forKey: "StudentModel")
        }
    }
    
    static func restore() -> StudentModel?{
        if let json = UserDefaults.standard.object(forKey: "StudentModel") as? String {
            let jsonDecode = try! JSONDecoder().decode(StudentModel.self, from: json.data(using: .utf8)!)
            return jsonDecode
        }
        return nil
    }
}

struct TeacherModel: Codable {
    let user: AccountModel
    let unitCol: OrgModel
    let department: String
    let timeCase: String
    let jobTitle: String
    
    private enum CodingKeys: String, CodingKey {
        case user
        case unitCol = "unit_col"
        case department
        case timeCase = "time_case"
        case jobTitle = "job_title"
    }
    
    func save() {
        let jsonEncode = try? JSONEncoder().encode(self)
        if let data = jsonEncode, let json = String(data: data, encoding: .utf8) {
            UserDefaults.standard.set(json, forKey: "TeacherModel")
        }
    }
    
    static func restore() -> TeacherModel?{
        if let json = UserDefaults.standard.object(forKey: "TeacherModel") as? String {
            let jsonDecode = try! JSONDecoder().decode(TeacherModel.self, from: json.data(using: .utf8)!)
            return jsonDecode
        }
        return nil
    }
}
