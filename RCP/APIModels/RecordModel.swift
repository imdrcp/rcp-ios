//
//  RecordModel.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/11/2.
//  Copyright © 2018 gs1. All rights reserved.
//

import Foundation

struct RecordModel: Decodable {
    let recordType: String
    let sectionTime: SectionTimeModel
    let student: StudentModel
    let recordDate: String
    
    private enum CodingKeys: String, CodingKey {
        case recordType = "record_type"
        case sectionTime = "section_time"
        case student
        case recordDate = "record_date"
    }
    
}
