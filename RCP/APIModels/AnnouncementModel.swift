//
//  AnnouncementModel.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/11/2.
//  Copyright © 2018 gs1. All rights reserved.
//

import Foundation


struct AnnouncementModel: Codable {
    let id: Int
    let postDate: String
    let title: String
    let content: String
    let endDate: String
    
    private enum CodingKeys: String, CodingKey {
        case id
        case postDate = "post_date"
        case title
        case content
        case endDate = "end_date"
    }
    
}
