//
//  BaseAPI.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/10/23.
//  Copyright © 2018 gs1. All rights reserved.
//

import Moya


protocol BaseAPI {
    var baseURL: URL {get}
    var headers: [String : String]? {get}
    var sampleData: Data {get}
}

extension BaseAPI {
    var sampleData: Data {
        return Data(count: 0)
    }
    
    var baseURL: URL {
        let host: String = ((UIApplication.shared.delegate as! AppDelegate).config?.host)!
        return URL(string: host)!
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json","Authorization": "Bearer \(Authentication.instance.token)"]
    }
}

public extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}
