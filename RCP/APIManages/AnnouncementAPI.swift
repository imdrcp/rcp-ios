//
//  AnnouncementAPI.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/11/2.
//  Copyright © 2018 gs1. All rights reserved.
//

import Moya

enum AnnouncementAPI {
    case posts
    case postDetail(Int)
}


extension AnnouncementAPI: TargetType, BaseAPI {
    var path: String {
        switch self {
        case .posts:
            return "/announcement/posts/"
        case .postDetail(let id):
            return "/announcement/posts/\(id)/"
        }
    }
    
    var method: Moya.Method {
        switch self {
        default:
            return .get
        }
    }
    
    var sampleData: Data {
        return "1234".data(using: .utf8)!
    }
    
    var task: Task {
        switch self {
        default:
            return .requestPlain
        }
    }
}
