//
//  SubjectsAPI.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/7.
//  Copyright © 2018年 gs1. All rights reserved.
//

import Foundation
import UIKit
import Moya


enum SubjectsAPI {
    case getSubjectsList
}

extension SubjectsAPI: TargetType, BaseAPI {
    
    var path: String {
        switch self {
        case .getSubjectsList:
            return "/curriculum/subjects/"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getSubjectsList:
            return .get
        }
    }
    
    var sampleData: Data {
        return "1234".data(using: .utf8)!
    }
    
    var task: Task {
        switch self {
        default:
            return .requestPlain
        }
    }
    
}

extension SubjectsAPI: AccessTokenAuthorizable{
    var authorizationType: AuthorizationType {
        switch self {
        default:
            return .bearer
        }
    }
}
