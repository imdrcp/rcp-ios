//
//  UserInfoAPI.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/10/23.
//  Copyright © 2018 gs1. All rights reserved.
//

import Moya

enum UserInfoAPI {
    case userDetail
    case teacherDetail(Int)
    case studentDetail(Int)
}


extension UserInfoAPI: TargetType, BaseAPI {
    var path: String {
        switch self {
        case .userDetail:
            return "/account/users/current_detail/"
        case .teacherDetail(let userId):
            return "/account/teacher_detail/\(userId)/"
        case .studentDetail(let userId):
            return "/account/student_detail/\(userId)/"
        }
    }
    
    var method: Moya.Method {
        switch self {
        default:
            return .get
        }
    }
    
    var sampleData: Data {
        return "1234".data(using: .utf8)!
    }
    
    var task: Task {
        switch self {
        default:
            return .requestPlain
        }
    }
}
