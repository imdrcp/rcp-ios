//
//  RollCallAPI.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/11/2.
//  Copyright © 2018 gs1. All rights reserved.
//

import Moya


enum RollCallAPI {
    case createCheck(student: Int, beacon: Int, sectionTime: Int)
    case records
    case beacons
}


extension RollCallAPI: TargetType, BaseAPI {
    var path: String {
        switch self {
        case .createCheck:
            return "/roll_call/roll_call_check/"
        case .records:
            return "/roll_call/roll_call_record/"
        case .beacons:
            return "/roll_call/beacons/"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .createCheck:
            return .post
        default:
            return .get
        }
    }
    
    var sampleData: Data {
        return "1234".data(using: .utf8)!
    }
    
    var task: Task {
        switch self {
        case .createCheck(let student, let beacon, let sectionTime):
            var parameters = [String: Any]()
            parameters["student"] = student
            parameters["beacon"] = beacon
            parameters["section_time"] = sectionTime
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        default:
            return .requestPlain
        }
    }
}
