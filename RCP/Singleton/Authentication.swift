//
//  Authentication.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/6/5.
//  Copyright © 2018年 gs1. All rights reserved.
//
import UIKit
import Alamofire
import Moya
import KeychainSwift
import CryptoSwift
import Hydra
import RxCocoa
import RxSwift

fileprivate enum AuthKey: String {
    case access = "access"
    case refresh = "refresh"
    case username = "xxx-username"
    case password = "xxx-password"
    case userId = "xxx-user_id"
}

final class Authentication {
    static let instance = Authentication()
    
    var isTeacher: Bool = false
    var isStudent: Bool = false
    
    var token: String {
        get {
            if isAuth {
                return accessToken
            }
            return ""
        }
    }
    var isAuth: Bool = false
    
    private(set) var userId: Int? {
        get {
            guard let id = keychain.get(.userId) else {
                return nil
            }
            return Int(id)
        }
        set(newId) {
            guard let id = newId else {
                return
            }
            _ = keychain.set("\(id)", forKey: .userId)
        }
    }
    
    private var accessToken: String {
        get {
            return keychain.get(.access) ?? ""
        }
        set {
            _ = self.keychain.set(newValue, forKey: .access)
        }
    }
    private var refreshToken: String {
        get {
            return keychain.get(.refresh) ?? ""
        }
        set {
            _ = self.keychain.set(newValue, forKey: .refresh)
        }
    }
    private let keychain = KeychainSwift()
    private let host: String!
    private var disposeBag = DisposeBag()
    
    lazy var profile = ProfileShared.instance
    
    init() {
//        keychain.clear()
        host = (UIApplication.shared.delegate as! AppDelegate).config?.host
        if let access = keychain.get(.access), let refresh = keychain.get(.refresh) {
            accessToken = access
            refreshToken = refresh
            isAuth = true
        }else {
            isAuth = false
        }
    }
    
    func login(username: String, password: String, isRemember: Bool = false) {
        let cryptoPwd = cryptoPassword(password)
        if isRemember {
            let _ = keychain.set(username, forKey: .username)
            let _ = keychain.set(cryptoPwd, forKey: .password)
        }
        print(["username": username, "password": cryptoPwd])
        getTokens(body: ["username": username, "password": cryptoPwd]).then {(data) in
                self.isAuth = true
                let json : [String: String] = data as! [String : String]
                self.accessToken = json["access"] ?? ""
                self.refreshToken = json["refresh"] ?? ""
                self.loadUserInfo()
            }.catch { (error) in
                self.isAuth = false
            }
    }
    
    private func cryptoPassword(_ password: String)-> String {
        return password.sha256()
    }
    
    private func getTokens(body: [String: String])-> Promise<Any>
    {
        let headers: HTTPHeaders = ["Content-Type": "application/json"]
        return Promise<Any>(in: .main, { (resolve, reject, _) in
            Alamofire.request( urls.token.path(self.host), method: .post, parameters: body,
                               encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                                switch response.result{
                                case .success(let data):
                                    if let status = response.response?.statusCode, status >= 400 {
                                        reject(AuthError.badRequest)
                                    }else{
                                        resolve(data)
                                    }
                                case .failure(let error):
                                    reject(error)
                                }
            }
        }).timeout(timeout: 60.0)
    }
    
    func refresh(){
        let headers: HTTPHeaders = ["Content-Type": "application/json"]
        Promise<[String: String]>(in: Context.userInteractive, { (resolve, reject, _) in
            Alamofire.request( urls.refresh.path(self.host), method: .post,
                               parameters: ["refresh": self.refreshToken],
                               encoding: JSONEncoding.default,
                               headers: headers).responseJSON { (response) in
                                switch response.result{
                                case .success(let data):
                                    if let status = response.response?.statusCode, status >= 400 {
                                        reject(AuthError.badRequest)
                                    }else{
                                     resolve(data as! [String : String])
                                    }
                                case .failure(let error):
                                    reject(error)
                                }
            }
        }).then { (json) in
            self.accessToken = json["access"]!
        }.catch { (error) in
                self.isAuth = false
        }
    }
    
    func getVerify()-> Promise<Bool> {
        let headers: HTTPHeaders = ["Content-Type": "application/json"]
        return Promise<Bool>(in: Context.main, { (resolve, reject, _) in
            Alamofire.request( urls.verify.path(self.host), method: .post,
                               parameters: ["token": self.refreshToken],
                               encoding: JSONEncoding.default,
                               headers: headers).responseJSON { (response) in
                                switch response.result{
                                case .success:
                                    if let status = response.response?.statusCode, status >= 400 {
                                        resolve(false)
                                    }else{
                                        resolve(true)
                                    }
                                case .failure:
                                     resolve(false)
                                }
            }
        })
        
    }
    
    func loadUserInfo() {
        profile.userInfo
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .filter{$0 != nil}
            .map{$0!.id}
            .subscribe(onNext: {[unowned self] in self.userId = $0})
            .disposed(by: disposeBag)
        profile.student.asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .map{$0 != nil}
            .subscribe(onNext: {[unowned self] in self.isStudent = $0})
            .disposed(by: disposeBag)
        profile.teacher.asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .map{$0 != nil}
            .subscribe(onNext: {[unowned self] in self.isTeacher = $0})
            .disposed(by: disposeBag)
    }
    
    private enum urls :String {
        case token = "/token/"
        case refresh = "/token/refresh/"
        case verify = "/token/verify/"
        
        func path(_ host: String)-> String {
            return "\(host)\(self.rawValue)"
        }
    }
    
    enum AuthError:Error {
        case badRequest
        case notFound
        case notAccessToken
        case notRefreshToken
    }
    
}

fileprivate extension KeychainSwift {
    func get(_ key: AuthKey)-> String? {
        return self.get(key.rawValue)
    }
    
    func getData(_ key: AuthKey)-> Data? {
        return self.getData(key.rawValue)
    }
    
    func getBool(_ key: AuthKey)-> Bool? {
        return self.getBool(key.rawValue)
    }
    
    func set(_ value: String, forKey key: AuthKey) -> Bool {
        return self.set(value, forKey: key.rawValue)
    }
    
    func set(_ value: Data, forKey key: AuthKey) -> Bool {
        return self.set(value, forKey: key.rawValue)
    }
    
    func set(_ value: Bool, forKey key: AuthKey) -> Bool {
        return self.set(value, forKey: key.rawValue)
    }
}
