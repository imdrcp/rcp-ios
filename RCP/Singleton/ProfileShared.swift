//
//  ProfileViewModel.swift
//  RCP
//
//  Created by 謝佳瑋 on 2018/10/27.
//  Copyright © 2018 gs1. All rights reserved.
//

import Moya
import RxSwift
import RxCocoa


class ProfileShared {
    
    static let instance = ProfileShared()
    
    let userInfo = BehaviorRelay<AccountModel?>(value: nil)
    let student = BehaviorRelay<StudentModel?>(value: nil)
    let teacher = BehaviorRelay<TeacherModel?>(value: nil)
    
    private var apiService = MoyaProvider<UserInfoAPI>()
    private var disposeBag = DisposeBag()
    
    private lazy var userResponse = apiService.rx.request(.userDetail)
                                                .retry(3)
                                                .filterSuccessfulStatusCodes()
                                                .map(AccountModel?.self)
                                                .asObservable()
                                                .share(replay: 1, scope: .whileConnected)
                                                .catchErrorJustReturn(nil)
    
    init() {
        loadUserInfo()
    }
    
    
    private func loadUserInfo() {
        
        userResponse.bind(to: userInfo).disposed(by: disposeBag)
        userInfo.asObservable()
            .filter{$0 != nil}
            .map{$0!.id}
            .flatMap{[unowned self] in
                self.apiService.rx.request(.studentDetail($0))
                    .filterSuccessfulStatusCodes()
                    .map(StudentModel?.self)
                    .catchErrorJustReturn(nil)
            }.bind(to: student).disposed(by: disposeBag)
        userInfo.asObservable()
            .filter{$0 != nil}
            .map{$0!.id}
            .flatMap{[unowned self] in
                self.apiService.rx.request(.teacherDetail($0))
                    .filterSuccessfulStatusCodes()
                    .map(TeacherModel?.self)
                    .catchErrorJustReturn(nil)
            }.bind(to: teacher).disposed(by: disposeBag)
    }
}
